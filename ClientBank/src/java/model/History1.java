/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
public class History1 implements Serializable {


    private Integer id;
   
    private String ordercode;
   
    private String time;
   
    private String  nametransfer;
    private String  idtransfer;
   private Integer transactionamount;
    private String idreceived;
      private String namereceived;

    public History1() {
    }

    public History1(Integer id, String ordercode, String time, String nametransfer, String idtransfer, Integer transactionamount, String idreceived, String namereceived) {
        this.id = id;
        this.ordercode = ordercode;
        this.time = time;
        this.nametransfer = nametransfer;
        this.idtransfer = idtransfer;
        this.transactionamount = transactionamount;
        this.idreceived = idreceived;
        this.namereceived = namereceived;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrdercode() {
        return ordercode;
    }

    public void setOrdercode(String ordercode) {
        this.ordercode = ordercode;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNametransfer() {
        return nametransfer;
    }

    public void setNametransfer(String nametransfer) {
        this.nametransfer = nametransfer;
    }

    public String getIdtransfer() {
        return idtransfer;
    }

    public void setIdtransfer(String idtransfer) {
        this.idtransfer = idtransfer;
    }

    public Integer getTransactionamount() {
        return transactionamount;
    }

    public void setTransactionamount(Integer transactionamount) {
        this.transactionamount = transactionamount;
    }

    public String getIdreceived() {
        return idreceived;
    }

    public void setIdreceived(String idreceived) {
        this.idreceived = idreceived;
    }

    public String getNamereceived() {
        return namereceived;
    }

    public void setNamereceived(String namereceived) {
        this.namereceived = namereceived;
    }

  
      
      
}
