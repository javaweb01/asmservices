/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "History.findAll", query = "SELECT h FROM History h")
    , @NamedQuery(name = "History.findById", query = "SELECT h FROM History h WHERE h.id = :id")
    , @NamedQuery(name = "History.findByOrdercode", query = "SELECT h FROM History h WHERE h.ordercode = :ordercode")
    , @NamedQuery(name = "History.findByTransactionamount", query = "SELECT h FROM History h WHERE h.transactionamount = :transactionamount")
    , @NamedQuery(name = "History.findByTime", query = "SELECT h FROM History h WHERE h.time = :time")})
public class History implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 255)
    @Column(name = "ORDERCODE")
    private String ordercode;
    @Column(name = "TRANSACTIONAMOUNT")
    private Integer transactionamount;
    @Column(name = "TIME")
    @Temporal(TemporalType.DATE)
    private Date time;
    @JoinColumn(name = "IDTRANSFER", referencedColumnName = "ID")
    @ManyToOne
    private Account idtransfer;
    @JoinColumn(name = "IDRECEIVED", referencedColumnName = "ID")
    @ManyToOne
    private Account idreceived;

    public History() {
    }

    public History(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrdercode() {
        return ordercode;
    }

    public void setOrdercode(String ordercode) {
        this.ordercode = ordercode;
    }

    public Integer getTransactionamount() {
        return transactionamount;
    }

    public void setTransactionamount(Integer transactionamount) {
        this.transactionamount = transactionamount;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Account getIdtransfer() {
        return idtransfer;
    }

    public void setIdtransfer(Account idtransfer) {
        this.idtransfer = idtransfer;
    }

    public Account getIdreceived() {
        return idreceived;
    }

    public void setIdreceived(Account idreceived) {
        this.idreceived = idreceived;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof History)) {
            return false;
        }
        History other = (History) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.History[ id=" + id + " ]";
    }
    
}
