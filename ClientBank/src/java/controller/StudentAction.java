/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import client.history;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;
 
import com.opensymphony.xwork2.ActionSupport;
import java.io.StringReader;
import java.util.LinkedList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import model.History;
import model.History1;
 
import model.Student;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
 
public class StudentAction extends ActionSupport {
    private List<History1> listStudents;
 
    /**
     * init and show listStudents to View
     * 
     * @author viettuts.vn
     * @return SUCCESS
     */
    
    
    public String execute() throws Exception {
       listStudents = new ArrayList<History1>();
       listStudents=geths();
        return SUCCESS;
    }

     
    /**
     * delete student action
     * 
     * @author viettuts.vn
     * @return SUCCESS
     */
   
     
    // getter and setter
    public List<History1> getListStudents() {
        return listStudents;
    }
     
    public void setListStudents(List<History1> listStudents) {
        this.listStudents = listStudents;
    }

    
    public List<History1> geths() throws Exception{
    
      history s=new history();
     String a= s.getByTimeEND("1998-12-31", "2018-01-01");
        //System.out.println(a);
    DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    InputSource is = new InputSource();
     
    is.setCharacterStream(new StringReader(a));

    Document doc = db.parse(is);
    NodeList nodes = doc.getElementsByTagName("history");
    List<History1> ls=new LinkedList<History1>();
   
    for (int i = 0; i < nodes.getLength(); i++) {
         History1 history=new History1();
      Element element = (Element) nodes.item(i);

      NodeList time = element.getElementsByTagName("time");
      Element line = (Element) time.item(0);
     history.setTime((getCharacterDataFromElement(line)));
    
    

      NodeList id = element.getElementsByTagName("id");
      line = (Element) id.item(0);
      history.setId(1);
     
      
      NodeList ordercode = element.getElementsByTagName("ordercode");
      line = (Element) ordercode.item(0);
    history.setOrdercode(getCharacterDataFromElement(line));
      
      NodeList transactionamount = element.getElementsByTagName("transactionamount");
      line = (Element) transactionamount.item(0);
     history.setTransactionamount(Integer.parseInt(getCharacterDataFromElement(line)));
    
      
        NodeList idreceived = element.getElementsByTagName("idreceived");
        model.Account account=new model.Account();
        for (int k = 0; k < idreceived.getLength(); k++) {
        Element element1 = (Element) idreceived.item(k);

        NodeList time1 = element1.getElementsByTagName("id");
        Element line1 = (Element) time1.item(0);
       account.setId(Integer.parseInt(getCharacterDataFromElement(line1)));
        
        
         NodeList cardnumber = element1.getElementsByTagName("cardnumber");
         line1 = (Element) cardnumber.item(0);
        account.setCardnumber(getCharacterDataFromElement(line1));
        
        
         NodeList expirydate = element1.getElementsByTagName("expirydate");
         line1 = (Element) expirydate.item(0);
        account.setExpirydate(getCharacterDataFromElement(line1));
        
          NodeList namecard = element1.getElementsByTagName("namecard");
         line1 = (Element) namecard.item(0);
        account.setNamecard(getCharacterDataFromElement(line1));
        
          NodeList pincode = element1.getElementsByTagName("pincode");
         line1 = (Element) pincode.item(0);
          account.setPincode(getCharacterDataFromElement(line1));
         }
     history.setIdreceived(account.getCardnumber());
    history.setNamereceived(account.getNamecard());
   
     
 
     NodeList idtransfer = element.getElementsByTagName("idtransfer");
        model.Account account2=new model.Account();
        for (int k = 0; k < idreceived.getLength(); k++) {
        Element element2 = (Element) idtransfer.item(k);

        NodeList time1 = element2.getElementsByTagName("id");
        Element line1 = (Element) time1.item(0);
       account2.setId(Integer.parseInt(getCharacterDataFromElement(line1)));
        
        
         NodeList cardnumber = element2.getElementsByTagName("cardnumber");
         line1 = (Element) cardnumber.item(0);
        account2.setCardnumber(getCharacterDataFromElement(line1));
        
        
         NodeList expirydate = element2.getElementsByTagName("expirydate");
         line1 = (Element) expirydate.item(0);
        account2.setExpirydate(getCharacterDataFromElement(line1));
        
          NodeList namecard = element2.getElementsByTagName("namecard");
         line1 = (Element) namecard.item(0);
        account2.setNamecard(getCharacterDataFromElement(line1));
        
          NodeList pincode = element2.getElementsByTagName("pincode");
         line1 = (Element) pincode.item(0);
          account2.setPincode(getCharacterDataFromElement(line1));
         }
     history.setIdtransfer(account2.getCardnumber());
     history.setNametransfer(account2.getNamecard());
     
     ls.add(history);
     
    }
        
        return ls;
        
        
  }

  public static String getCharacterDataFromElement(Element e) {
    Node child = e.getFirstChild();
    if (child instanceof CharacterData) {
      CharacterData cd = (CharacterData) child;
      return cd.getData();
    }
    return "";
  }
}
