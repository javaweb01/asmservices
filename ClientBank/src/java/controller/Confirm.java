/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import client.account;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class Confirm extends ActionSupport {
    
    public Confirm() {
    }
    String cardnumber;
    String cardname;
    String ordercode;
    String transactionamount;
    int transactionfee=0;

    public int getTransactionfee() {
        return transactionfee;
    }

    public void setTransactionfee(int transactionfee) {
        this.transactionfee = transactionfee;
    }
    
    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getCardname() {
        return cardname;
    }

    public void setCardname(String cardname) {
        this.cardname = cardname;
    }

    public String getOrdercode() {
        return ordercode;
    }

    public void setOrdercode(String ordercode) {
        this.ordercode = ordercode;
    }

    public String getTransactionamount() {
        return transactionamount;
    }

    public void setTransactionamount(String transactionamount) {
        this.transactionamount = transactionamount;
    }
 
    
    public String execute() throws Exception {
        
        Map session = ActionContext.getContext().getSession(); 
        cardnumber=  (String) session.get("cardnumber"); 
        model.Account account=new account().getByNumber(cardnumber);
        cardname=account.getNamecard();
        session.put("cardname", cardname);
        transactionamount=  (String) session.get("transactionamount");
        int fee=Integer.parseInt(transactionamount);
        
        
        if(fee<=100000){
            transactionfee=1000;
        }
        
          if(fee>100000&&fee<=500000){
              
            transactionfee=(fee/100)*2;
            }
          
          if(fee>500000&&fee<=1000000){
            transactionfee=(fee/100)*(3/2);
            }
          if(fee>1000000||fee<=5000000){
            transactionfee=(fee/100);
            }
          if(fee>5000000){
              transactionfee=(fee/100)*1/2;
            }
           session.put("transactionfee", transactionfee+"");
               
        return SUCCESS;
    }
    
}
