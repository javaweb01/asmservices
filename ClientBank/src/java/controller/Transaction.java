/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Admin
 */
public class Transaction extends ActionSupport  {
    String ordercode;
    String transactionamount;
    String cardnumber;
   
    public Transaction() {
    }

    public String getOrdercode() {
        return ordercode;
    }

    public void setOrdercode(String ordercode) {
        this.ordercode = ordercode;
    }

    public String getTransactionamount() {
        return transactionamount;
    }

    public void setTransactionamount(String transactionamount) {
        this.transactionamount = transactionamount;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }
 


    public String execute() throws Exception {
         Map session = ActionContext.getContext().getSession(); 
        session.put("cardnumber", cardnumber);
        session.put("ordercode", ordercode);
        session.put("transactionamount", transactionamount);
        
        return SUCCESS;
    }


}
