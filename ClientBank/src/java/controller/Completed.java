/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.text.SimpleDateFormat;
import client.account;
import client.accountinformation;
import client.history;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import model.History;
import java.util.Date;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.GenericType;
import model.Accountinformation;
import org.xml.sax.SAXException;

/**
 *
 * @author Admin
 */
public class Completed extends ActionSupport {
    String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    
    public Completed() {
    }
    
    public String execute() throws Exception {
        
accountinformation();
history();
 Map session = ActionContext.getContext().getSession(); 
 session.remove("ordercode");
  session.remove("transactionfee");
   session.remove("transactionamount");
      session.remove("cardnumber");

       return SUCCESS;
    }
    public void history() throws Exception {
             Map session = ActionContext.getContext().getSession(); 
       String cardnumber=  (String) session.get("cardnumber"); 
       model.Account account=new account().getByNumber(cardnumber);
       String idpartner=account.getId().toString();
       String id= (String) session.get("id"); 
       String ordercode=  (String) session.get("ordercode"); 
       String transactionamount=  (String) session.get("transactionamount"); 
       String transactionfee=  (String) session.get("transactionfee"); 
     
   
       String sDate1="31/12/1998";  
       Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
       history hs=new history();
       History  history=new History();
       history.setId(1);
       history.setIdreceived(new model.Account(Integer.parseInt(idpartner)));
       history.setIdtransfer(new model.Account(Integer.parseInt(id)));
       history.setTime(date1);
       history.setOrdercode(ordercode);
       history.setTransactionamount(Integer.parseInt(transactionamount));
       hs.create_XML(history);  
    }
     
    public void accountinformation() throws Exception{
        
        Map session = ActionContext.getContext().getSession(); 
       String cardnumber=  (String) session.get("cardnumber"); 
       String transactionamount=  (String) session.get("transactionamount"); 
       String transactionfee=  (String) session.get("transactionfee"); 
       
       model.Account account=new account().getByNumber(cardnumber);
       String idpartner=account.getId().toString();
       String id= (String) session.get("id");
        accountinformation af=new accountinformation();
       Accountinformation accountinformation=new Accountinformation();
        GenericType<Accountinformation>gt=new GenericType<Accountinformation>(){};
 
     //cộng
      
      
   accountinformation =af.find_XML(gt, idpartner);
   int balance=accountinformation.getBalance()+Integer.parseInt(transactionamount);
   Accountinformation acf=new Accountinformation();
   acf.setId(Integer.parseInt(idpartner));
   acf.setBalance(balance);
   acf.setAccount(new model.Account(Integer.parseInt(idpartner)));
   af.edit_XML(acf, idpartner);
     
     
     //trừ
     
   accountinformation =af.find_XML(gt, id);
   int balance1  =accountinformation.getBalance()-Integer.parseInt(transactionfee)-Integer.parseInt(transactionamount);
   acf.setId(Integer.parseInt(id));
   acf.setBalance(balance1);
   acf.setAccount(new model.Account(Integer.parseInt(id)));
   af.edit_XML(acf, id);
     

    }
}
