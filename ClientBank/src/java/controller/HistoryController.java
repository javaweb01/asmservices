/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import client.history;
import com.opensymphony.xwork2.ActionSupport;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import  model.History;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import java.util.List;
import java.util.LinkedList;

/**
 *
 * @author Admin
 */
public class HistoryController extends ActionSupport {
    
    public HistoryController() {
    }
    public List<History> history;
   public String dem;

    public String getDem() {
        return dem;
    }

    public void setDem(String dem) {
        this.dem = dem;
    }
    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }
    
    public String execute() throws Exception {
      List<History> hh=new LinkedList<History>();
       History h=new History();
  
      hh.add(h);
        setHistory(hh);
      return SUCCESS;
    }
 
    
    public List<History> geths() throws Exception{
    
      history s=new history();
     String a= s.getByTimeEND("2008-01-01", "2009-01-01");
        //System.out.println(a);
    DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    InputSource is = new InputSource();
     
    is.setCharacterStream(new StringReader(a));

    Document doc = db.parse(is);
    NodeList nodes = doc.getElementsByTagName("history");
    List<History> ls=new LinkedList<History>();
   
    for (int i = 0; i < nodes.getLength(); i++) {
         History history=new History();
      Element element = (Element) nodes.item(i);

      NodeList time = element.getElementsByTagName("time");
      Element line = (Element) time.item(0);
     

      NodeList id = element.getElementsByTagName("id");
      line = (Element) id.item(0);
      history.setId(1);
     
      
      NodeList ordercode = element.getElementsByTagName("ordercode");
      line = (Element) ordercode.item(0);
    history.setOrdercode(getCharacterDataFromElement(line));
      
      NodeList transactionamount = element.getElementsByTagName("transactionamount");
      line = (Element) transactionamount.item(0);
      history.setTransactionamount(Integer.parseInt(getCharacterDataFromElement(line)));
    
      
        NodeList idreceived = element.getElementsByTagName("idreceived");
        model.Account account=new model.Account();
        for (int k = 0; k < idreceived.getLength(); k++) {
        Element element1 = (Element) idreceived.item(k);

        NodeList time1 = element1.getElementsByTagName("id");
        Element line1 = (Element) time1.item(0);
       account.setId(Integer.parseInt(getCharacterDataFromElement(line1)));
        
        
         NodeList cardnumber = element1.getElementsByTagName("cardnumber");
         line1 = (Element) cardnumber.item(0);
        account.setCardnumber(getCharacterDataFromElement(line1));
        
        
         NodeList expirydate = element1.getElementsByTagName("expirydate");
         line1 = (Element) expirydate.item(0);
        account.setExpirydate(getCharacterDataFromElement(line1));
        
          NodeList namecard = element1.getElementsByTagName("namecard");
         line1 = (Element) namecard.item(0);
        account.setNamecard(getCharacterDataFromElement(line1));
        
          NodeList pincode = element1.getElementsByTagName("pincode");
         line1 = (Element) pincode.item(0);
          account.setPincode(getCharacterDataFromElement(line1));
         }
     history.setIdreceived(account);
   
     
 
     NodeList idtransfer = element.getElementsByTagName("idtransfer");
        model.Account account2=new model.Account();
        for (int k = 0; k < idreceived.getLength(); k++) {
        Element element2 = (Element) idtransfer.item(k);

        NodeList time1 = element2.getElementsByTagName("id");
        Element line1 = (Element) time1.item(0);
       account2.setId(Integer.parseInt(getCharacterDataFromElement(line1)));
        
        
         NodeList cardnumber = element2.getElementsByTagName("cardnumber");
         line1 = (Element) cardnumber.item(0);
        account2.setCardnumber(getCharacterDataFromElement(line1));
        
        
         NodeList expirydate = element2.getElementsByTagName("expirydate");
         line1 = (Element) expirydate.item(0);
        account2.setExpirydate(getCharacterDataFromElement(line1));
        
          NodeList namecard = element2.getElementsByTagName("namecard");
         line1 = (Element) namecard.item(0);
        account2.setNamecard(getCharacterDataFromElement(line1));
        
          NodeList pincode = element2.getElementsByTagName("pincode");
         line1 = (Element) pincode.item(0);
          account2.setPincode(getCharacterDataFromElement(line1));
         }
     history.setIdtransfer(account2);
     
     ls.add(history);
     
    }
        
        return ls;
  }

  public static String getCharacterDataFromElement(Element e) {
    Node child = e.getFirstChild();
    if (child instanceof CharacterData) {
      CharacterData cd = (CharacterData) child;
      return cd.getData();
    }
    return "";
  }
}
