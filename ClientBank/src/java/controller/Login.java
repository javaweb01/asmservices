/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import client.account;
import model.Account;
import com.opensymphony.xwork2.ActionSupport;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.ws.rs.core.GenericType;
import model.History;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Admin
 */
public class Login extends ActionSupport implements SessionAware {
    
    public Login() {
    }
    String namecart;
     String cardnumber;
      String expirydate;
        String securitycode;
   public Map<String, Object> session;
    public String execute() throws Exception{ 
        Account account=new Account();
        account ac=new account();
//        ac.setId(1);
//        ac.setIdreceived(new model.Account(1));
//        ac.setIdtransfer(new model.Account(1));
//        ac.setTransactionamount(123123);
//        String sDate1="31/12/1998";  
//        Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1); 
//        history.setTime(date1);
//       account.setCardnumber(cardnumber);
//       account.setId(1);
//       account.setPincode(securitycode);
String check =ac.checklogin(cardnumber, namecart, securitycode, expirydate);
        if(!check.equals("null")){
             GenericType<Account>gt=new GenericType<Account>(){};
      
         account=  ac.find_XML(gt, check);
             session.put("id", account.getId().toString());
             //session.put("cardnumber", account.getCardnumber());
            return SUCCESS;
        }else{
            return ERROR;
        }
    }

    public String getNamecart() {
        return namecart;
    }

    public void setNamecart(String namecart) {
        this.namecart = namecart;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getSecuritycode() {
        return securitycode;
    }

    public void setSecuritycode(String securitycode) {
        this.securitycode = securitycode;
    }
    
     @Override
    public void setSession(Map<String, Object> map) {
    this.session=map;  }
    
}
