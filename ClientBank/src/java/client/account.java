/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import java.io.StringReader;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import model.Account;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Jersey REST client generated for REST resource:AccountFacadeREST
 * [entity.account]<br>
 * USAGE:
 * <pre>
 *        account client = new account();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author Admin
 */
public class account {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:24919/ServivesBank/webresources";

    public account() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("entity.account");
    }

    public String countREST() throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("count");
        return resource.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
    }

    public void edit_XML(Object requestEntity, String id) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("{0}", new Object[]{id})).request(javax.ws.rs.core.MediaType.APPLICATION_XML).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_XML));
    }

    public void edit_JSON(Object requestEntity, String id) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("{0}", new Object[]{id})).request(javax.ws.rs.core.MediaType.APPLICATION_JSON).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON));
    }

    public <T> T find_XML(GenericType<T> responseType, String id) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("{0}", new Object[]{id}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public <T> T find_JSON(Class<T> responseType, String id) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("{0}", new Object[]{id}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public <T> T findRange_XML(Class<T> responseType, String from, String to) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("{0}/{1}", new Object[]{from, to}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public <T> T findRange_JSON(Class<T> responseType, String from, String to) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("{0}/{1}", new Object[]{from, to}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void create_XML(Object requestEntity) throws ClientErrorException {
        webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_XML).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_XML));
    }

    public void create_JSON(Object requestEntity) throws ClientErrorException {
        webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON));
    }

 

    public <T> T findAll_XML(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public <T> T findAll_JSON(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void remove(String id) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("{0}", new Object[]{id})).request().delete();
    }


    public void close() {
        client.close();
    }
    

     public String checklogin(String cardnumber,String namecard,String pincode,String expirydate ) throws ClientErrorException {
        WebTarget resource = webTarget;
       
        resource = resource.queryParam("cardnumber", cardnumber);
         resource = resource.queryParam("namecard", namecard);
          resource = resource.queryParam("pincode", pincode);
           resource = resource.queryParam("expirydate", expirydate);
        resource = resource.path("checklogin");
        return resource.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
    }
     
     
 
       
       
  public Account getByNumber(String cardnumber) throws ClientErrorException, SAXException, IOException, ParserConfigurationException {
       WebTarget resource = webTarget;
       
        resource = resource.queryParam("cardnumber", cardnumber);
       
      
        resource = resource.path("getByNumber");
        return getXML(resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(String.class));
    }
  
    
    
    public Account getXML(String xml) throws SAXException, IOException, ParserConfigurationException{
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    InputSource is = new InputSource();
     
    is.setCharacterStream(new StringReader(xml));

    Document doc = db.parse(is);
    NodeList nodes = doc.getElementsByTagName("account");
    
    Account account=new Account();
    for (int i = 0; i < nodes.getLength(); i++) {
      Element element = (Element) nodes.item(i);

      NodeList namecard = element.getElementsByTagName("namecard");
      Element line = (Element) namecard.item(0);
      account.setNamecard(getCharacterDataFromElement(line));
      
      NodeList id = element.getElementsByTagName("id");
      line = (Element) id.item(0);
      account.setId(Integer.parseInt(getCharacterDataFromElement(line)));
      
       NodeList cardnumber = element.getElementsByTagName("cardnumber");
      line = (Element) cardnumber.item(0);
      account.setCardnumber(getCharacterDataFromElement(line));
      
      NodeList pincode = element.getElementsByTagName("pincode");
      line = (Element) pincode.item(0);
       account.setPincode(getCharacterDataFromElement(line));
     
      NodeList expirydate = element.getElementsByTagName("expirydate");
      line = (Element) expirydate.item(0);
       account.setExpirydate(getCharacterDataFromElement(line));
     
    }
    
    return account;
    }
    
      public static String getCharacterDataFromElement(Element e) {
    Node child = e.getFirstChild();
    if (child instanceof CharacterData) {
      CharacterData cd = (CharacterData) child;
      return cd.getData();
    }
    return "";
  }
}
