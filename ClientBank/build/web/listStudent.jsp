<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Struts2 - Example</title>
<style type="text/css">
.error-msg {
  color: red;
}
 
table.list {
  border: 1px solid #cbcbcb;
  border-collapse: collapse;
}
 
table.list td, th {
  border-left: 1px solid #cbcbcb;
  padding-left: 10px;
  padding-right: 10px;
}
 
table.list thead {
  background-color: #cbcbcb;
}
</style>
</head>
<body>
  <s:form id="studentForm" class="studentForm" theme="simple">
    <div class="error-msg">
      <s:actionerror />
    </div>
    <table class="list">
      <thead>
        <tr>
          <th>ID</th>
          <th>Mã Đơn Hàng</th>
          <th>Người Thanh Toán</th>
          <th>STK Thanh Toán </th>
          <th>Người Nhận</th>
          <th>STK Nhận </th>
          <th>Số Tiền</th>
          <th>Thời Gian</th>
          
          
          
        </tr>
      </thead>
      <!-- show list student -->
      <tbody>
        <s:iterator value="listStudents" id="student" status="st">
          <tr>
            <td><s:property value="#student.id" /></td>
            <td><s:property value="#student.ordercode" /></td>
            <td><s:property value="#student.namereceived" /></td>
            <td><s:property value="#student.idreceived" /></td>
            <td><s:property value="#student.nametransfer" /></td>
             <td><s:property value="#student.idtransfer" /></td>
              <td><s:property value="#student.transactionamount" /></td>
               <td><s:property value="#student.time" /></td>
             
           
            
          </tr>
        </s:iterator>
      </tbody>
    </table>
    <div class="control-btn">
      <s:submit name="delete" value="Delete"
                onclick="this.form.action='student_delete'" />
    </div>
  </s:form>
</body>
</html>
