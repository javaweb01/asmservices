package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<div class=\"notes-wrapper\">\n");
      out.write("\t<div class=\"saver-wrap\" contenteditable=\"false\">\n");
      out.write("\t\t<div class=\"notes-dot\" contenteditable=\"false\"></div>\n");
      out.write("\t\t<div class=\"rest\" contenteditable=\"false\"></div>\n");
      out.write("\t</div>\n");
      out.write("\t<div class=\"title\" contenteditable=\"true\" onpaste=\"OnPaste_StripFormatting(this, event);\">\n");
      out.write("\t\tnote title\n");
      out.write("\t</div>\n");
      out.write("\t<div class=\"content\" contenteditable=\"true\" onpaste=\"OnPaste_StripFormatting(this, event);\">\n");
      out.write("\tStart typing...\n");
      out.write("\t</div>\n");
      out.write("\t<div class=\"line\"></div>\n");
      out.write("\t<div class=\"comment\"></div>\n");
      out.write("\t<input type=\"file\" onchange=\"previewFile()\" id=\"selectedFile\" style=\"display: none;\" />\n");
      out.write("\t<div class=\"getImg\" onclick=\"document.getElementById('selectedFile').click();\">\n");
      out.write("\t\t<i class=\"material-icons\">insert_photo</i>\n");
      out.write("\t</div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("<style>\n");
      out.write("    \n");
      out.write("    .notes-wrapper {\n");
      out.write("    max-width: 400px;\n");
      out.write("    width: 90%;\n");
      out.write("    margin: 0 auto;\n");
      out.write("    height: auto;\n");
      out.write("    background-color: #fff;\n");
      out.write("    border-radius: 5px;\n");
      out.write("    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);\n");
      out.write("    position: relative;\n");
      out.write("    z-index: 999;\n");
      out.write("}\n");
      out.write("\n");
      out.write("    </style>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
