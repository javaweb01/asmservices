/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Account;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Admin
 */
@Stateless
@Path("entity.account")
public class AccountFacadeREST extends AbstractFacade<Account> {

    @PersistenceContext(unitName = "ServivesBankPU")
    private EntityManager em;

    public AccountFacadeREST() {
        super(Account.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Account entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Account entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Account find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Account> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Account> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    

    @GET
    @Path("checklogin")
    @Consumes({"application/xml","application/json"})
    public String checklogin(@QueryParam("cardnumber") String cardnumber,@QueryParam("pincode") String pincode,@QueryParam("namecard") String namecard,@QueryParam("expirydate") String expirydate) {
      Account us=new Account();
        try{
              us=(Account) em.createNamedQuery("Account.login")
                      .setParameter("cardnumber", cardnumber)
                      .setParameter("pincode", pincode)
                      .setParameter("namecard", namecard)
                      .setParameter("expirydate", expirydate)
                      .getSingleResult();
      
        }catch(Exception ex){
            
        }
        if (us.getId()!=null){
            return us.getId()+"";
        }else{
            return null;
        }
    }

    
    @GET
    @Path("getByNumber")
    @Consumes({"application/xml","application/json"})
    public Account getByNumber(@QueryParam("cardnumber") String cardnumber) {
      Account us=new Account();
        try{
              us=(Account) em.createNamedQuery("Account.findByCardnumber")
                      .setParameter("cardnumber", cardnumber)
                      .getSingleResult();
      
        }catch(Exception ex){
            
        }
        if (us.getId()!=null){
            return us;
        }else{
            return us;
        }
    }
    
}
