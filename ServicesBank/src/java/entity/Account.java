/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ACCOUNT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a")
    , @NamedQuery(name = "Account.findById", query = "SELECT a FROM Account a WHERE a.id = :id")
    , @NamedQuery(name = "Account.findByNamecard", query = "SELECT a FROM Account a WHERE a.namecard = :namecard")
    , @NamedQuery(name = "Account.findByCardnumber", query = "SELECT a FROM Account a WHERE a.cardnumber = :cardnumber")
    , @NamedQuery(name = "Account.findByPincode", query = "SELECT a FROM Account a WHERE a.pincode = :pincode")
    , @NamedQuery(name = "Account.login", query = "SELECT a FROM Account a WHERE a.namecard = :namecard AND a.pincode = :pincode AND a.cardnumber = :cardnumber AND a.expirydate = :expirydate")
    , @NamedQuery(name = "Account.findByExpirydate", query = "SELECT a FROM Account a WHERE a.expirydate = :expirydate")})
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 255)
    @Column(name = "NAMECARD")
    private String namecard;
    @Size(max = 255)
    @Column(name = "CARDNUMBER")
    private String cardnumber;
    @Size(max = 255)
    @Column(name = "PINCODE")
    private String pincode;
    @Size(max = 255)
    @Column(name = "EXPIRYDATE")
    private String expirydate;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "account")
    private Partneraccount partneraccount;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "account")
    private Accountinformation accountinformation;
    @OneToMany(mappedBy = "idtransfer")
    private List<History> historyList;
    @OneToMany(mappedBy = "idreceived")
    private List<History> historyList1;

    public Account() {
    }

    public Account(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNamecard() {
        return namecard;
    }

    public void setNamecard(String namecard) {
        this.namecard = namecard;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public Partneraccount getPartneraccount() {
        return partneraccount;
    }

    public void setPartneraccount(Partneraccount partneraccount) {
        this.partneraccount = partneraccount;
    }

    public Accountinformation getAccountinformation() {
        return accountinformation;
    }

    public void setAccountinformation(Accountinformation accountinformation) {
        this.accountinformation = accountinformation;
    }

    @XmlTransient
    public List<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
    }

    @XmlTransient
    public List<History> getHistoryList1() {
        return historyList1;
    }

    public void setHistoryList1(List<History> historyList1) {
        this.historyList1 = historyList1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Account)) {
            return false;
        }
        Account other = (Account) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Account[ id=" + id + " ]";
    }
    
}
